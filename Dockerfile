FROM osrf/ros:melodic-desktop-full

SHELL ["/bin/bash", "--login", "-c"]
RUN apt update
RUN apt install -y wget
RUN apt install -y python3-yaml tmux git
# Install miniconda
RUN mkdir -p ~/miniconda3
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
RUN chmod +x ~/miniconda3/miniconda.sh
RUN ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
RUN rm -rf ~/miniconda3/miniconda.sh

# Install dependencies
ENV PATH=/root/miniconda3/bin/:$PATH
RUN ~/miniconda3/bin/conda init bash
RUN conda create -y -n semantic_node python=3.7
SHELL ["conda", "run", "-n", "semantic_node", "/bin/bash", "-c"]
RUN pip install ultralytics
RUN pip install rospkg catkin_pkg tifffile requests catkin-tools empy

# Make catkin workspace
RUN mkdir -p /catkin_ws/src
WORKDIR /catkin_ws

# Configure catkin to use python3.7
# https://medium.com/@beta_b0t/how-to-setup-ros-with-python-3-44a69ca36674
RUN catkin config \
    -DPYTHON_EXECUTABLE=~/miniconda3/envs/semantic_node/bin/python3 \
    -DPYTHON_INCLUDE_DIR=~/miniconda3/envs/semantic_node/include/python3.7m \
    -DPYTHON_LIBRARY=~/miniconda3/envs/semantic_node/lib/libpython3.7m.so \
    -DSETUPTOOLS_DEB_LAYOUT=OFF
RUN catkin config --install

# Create folder structure 
RUN mkdir -p /catkin_ws/src/semantic_inference/scripts

# Handle cv_bridge issue
RUN git clone -b melodic https://github.com/ros-perception/vision_opencv.git \
    /catkin_ws/src/vision_opencv

# Deploy ROS node
COPY CMakeLists.txt /catkin_ws/src/semantic_inference/CMakeLists.txt
COPY msg /catkin_ws/src/semantic_inference/msg 
COPY package.xml /catkin_ws/src/semantic_inference/package.xml
COPY src/ros_node.py /catkin_ws/src/semantic_inference/scripts/
RUN chmod +x /catkin_ws/src/semantic_inference/scripts/ros_node.py

# Generate message
RUN source /ros_entrypoint.sh && catkin build

# Deploy weights here, so not to rebuild too much
ARG weight_file=yolov8m-seg.pt
COPY $weight_file /model_weights/weights.pt
WORKDIR /

COPY run.sh /run.sh
RUN chmod +x /run.sh
ENTRYPOINT [ "conda", "run", "--live-stream", "-n", "semantic_node", "bash", "/run.sh" ]


