#!/usr/bin/env bash
echo "Setting up ROS environment"
source /ros_entrypoint.sh
set +e 
source /catkin_ws/devel/setup.bash --extend

# TODO parametrize
echo 'Launching inference node...'
rosrun semantic_inference ros_node.py _segment_weights:=/model_weights/weights.pt