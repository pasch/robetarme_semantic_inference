from ultralytics import YOLO
from fastapi import FastAPI, APIRouter
from pydantic import BaseModel
import numpy as np
import base64 as b64
from io import BytesIO
from PIL import Image as PILImage
import tifffile
import cv2
import time

COCO_REBAR_BACKGROUND_CLS = {
    0: 'background',
    1: 'person',
    2: 'bicycle',
    3: 'car',
    4: 'motorcycle',
    5: 'airplane',
    6: 'bus',
    7: 'train',
    8: 'truck',
    9: 'boat',
    10: 'traffic light',
    11: 'fire hydrant',
    12: 'stop sign',
    13: 'parking meter',
    14: 'bench',
    15: 'bird',
    16: 'cat',
    17: 'dog',
    18: 'horse',
    19: 'sheep',
    20: 'cow',
    21: 'elephant',
    22: 'bear',
    23: 'zebra',
    24: 'giraffe',
    25: 'backpack',
    26: 'umbrella',
    27: 'handbag',
    28: 'tie',
    29: 'suitcase',
    30: 'frisbee',
    31: 'skis',
    32: 'snowboard',
    33: 'sports ball',
    34: 'kite',
    35: 'baseball bat',
    36: 'baseball glove',
    37: 'skateboard',
    38: 'surfboard',
    39: 'tennis racket',
    40: 'bottle',
    41: 'wine glass',
    42: 'cup',
    43: 'fork',
    44: 'knife',
    45: 'spoon',
    46: 'bowl',
    47: 'banana',
    48: 'apple',
    49: 'sandwich',
    50: 'orange',
    51: 'broccoli',
    52: 'carrot',
    53: 'hot dog',
    54: 'pizza',
    55: 'donut',
    56: 'cake',
    57: 'chair',
    58: 'couch',
    59: 'potted plant',
    60: 'bed',
    61: 'dining table',
    62: 'toilet',
    63: 'tv',
    64: 'laptop',
    65: 'mouse',
    66: 'remote',
    67: 'keyboard',
    68: 'cell phone',
    69: 'microwave',
    70: 'oven',
    71: 'toaster',
    72: 'sink',
    73: 'refrigerator',
    74: 'book',
    75: 'clock',
    76: 'vase',
    77: 'scissors',
    78: 'teddy bear',
    79: 'hair drier',
    80: 'toothbrush',
    81: 'ExposedBars'
}

app = FastAPI()

class Detection(BaseModel):
    cls: list
    boxes: list
    conf: list

class Segmentation(BaseModel):
    mask: list

class Image(BaseModel):
    data : str

class Mask(BaseModel):
    data : str | None = None
    time : dict

class Status(BaseModel):
    classes : dict
    weights: str
    seg_weights: str
    confidence: float

class Item(BaseModel):
    name: str
    description: str | None = None
    price: float
    tax: float | None = None

class InferenceEngine:
    def __init__(self):
        self.classes = COCO_REBAR_BACKGROUND_CLS
        self.confidence = 0.2
        self.weights = 'yolov8m.pt'
        # self.seg_weights = 'yolov8m-seg.pt'
        self.seg_weights = '/home/pasch/repos/rebar_training_pipeline/rebar_mask_training/train6/weights/best.pt'
        self.detector = YOLO(self.weights)
        self.segmentor = YOLO(self.seg_weights)
        self.router = APIRouter()
        self.router.add_api_route('/detect', self.detect, methods=['POST'])
        self.router.add_api_route('/segment', self.segment, methods=['POST'])
        self.router.add_api_route('/status', self.status, methods=['GET'])
        self.router.add_api_route('/', self.default, methods=['GET'])
        self.router.add_api_route('/item', self.create_item, methods=['POST'])

        #Warmup
        [self.segmentor(np.zeros((480, 640, 3), np.uint8)) for _ in range(3)]
    
    @staticmethod
    def decode_image(enc_img: str):
        dec_img = BytesIO(b64.b64decode(bytes(enc_img, encoding='utf8')))
        img = PILImage.open(dec_img)
        return img

    def detect(self, image: Image):
        pass

    def segment(self, image: Image):
        img = self.decode_image(image.data)
        result = self.segmentor(img)
        
        # (C, H, W)
        t1 = time.time()
        if result[0].masks is not None:
            predicted_masks = result[0].masks.data.cpu().numpy()
            predicted_classes = result[0].boxes.cpu().numpy().cls
            predicted_confs = result[0].boxes.cpu().numpy().conf
            mask = np.zeros((len(self.classes.keys()),
                                 *predicted_masks[0].shape))
            counts = np.zeros((len(self.classes.keys()),
                               *predicted_masks[0].shape),
                                dtype=np.uint32)
            for i, m in enumerate(predicted_masks):
                cls_id = predicted_classes[i].astype(int)
                mask[cls_id+1] += m*predicted_confs[i] #np.logical_or(mask[cls_id+1], m)
                counts[cls_id+1] += m.astype(np.uint32)
            
            # Avoid zero division
            counts[counts==0] = 1
            mask = mask / counts
            # Resize mask back to original size
            # Input: CHW, Result is H W C
            mask = cv2.resize(mask.transpose((1,2,0)), 
                              dsize=result[0].orig_shape[::-1])
            # Back to C, H, W
            mask = mask.transpose((2,0,1))
        else:
            mask = np.zeros((len(self.classes.keys()), img.height, img.width),
                        dtype=np.uint8)
        t2 = time.time()
        mask_assembly_time = t2-t1

        t1 = time.time()
        mask_buffer = BytesIO()
        tifffile.imwrite('test.tiff', mask.argmax(axis=0))
        tifffile.imwrite(mask_buffer, mask.argmax(axis=0))
        encoded_mask = b64.b64encode(mask_buffer.getvalue()).decode('utf8') 
        t2 = time.time()
        encoding_time = t2-t1

        return Mask(data=encoded_mask,
                    time={
                        'yolo' : result[0].speed,
                        'encoding': encoding_time*1000,
                        'assembly' : mask_assembly_time*1000
                    })
    
    def status(self):
        return Status(
            classes = self.classes,
            weights= self.weights,
            seg_weights = self.seg_weights,
            confidence = self.confidence
        )
    
    def default(self):
        return "Hello World!"

    def create_item(self, item: Item):
        return item

engine = InferenceEngine()
app.include_router(engine.router)
