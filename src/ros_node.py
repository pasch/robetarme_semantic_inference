#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from std_msgs.msg import UInt8MultiArray, Float32
import requests
from PIL import Image as PILImage
from io import BytesIO
from base64 import b64encode, b64decode
import tifffile
from rospy.numpy_msg import numpy_msg 
import numpy as np
# from semantic_inference.msg import UInt8Array
import colorsys
import time
import cv2

COCO_REBAR_BACKGROUND_CLS = {
    0: 'background',
    1: 'person',
    2: 'bicycle',
    3: 'car',
    4: 'motorcycle',
    5: 'airplane',
    6: 'bus',
    7: 'train',
    8: 'truck',
    9: 'boat',
    10: 'traffic light',
    11: 'fire hydrant',
    12: 'stop sign',
    13: 'parking meter',
    14: 'bench',
    15: 'bird',
    16: 'cat',
    17: 'dog',
    18: 'horse',
    19: 'sheep',
    20: 'cow',
    21: 'elephant',
    22: 'bear',
    23: 'zebra',
    24: 'giraffe',
    25: 'backpack',
    26: 'umbrella',
    27: 'handbag',
    28: 'tie',
    29: 'suitcase',
    30: 'frisbee',
    31: 'skis',
    32: 'snowboard',
    33: 'sports ball',
    34: 'kite',
    35: 'baseball bat',
    36: 'baseball glove',
    37: 'skateboard',
    38: 'surfboard',
    39: 'tennis racket',
    40: 'bottle',
    41: 'wine glass',
    42: 'cup',
    43: 'fork',
    44: 'knife',
    45: 'spoon',
    46: 'bowl',
    47: 'banana',
    48: 'apple',
    49: 'sandwich',
    50: 'orange',
    51: 'broccoli',
    52: 'carrot',
    53: 'hot dog',
    54: 'pizza',
    55: 'donut',
    56: 'cake',
    57: 'chair',
    58: 'couch',
    59: 'potted plant',
    60: 'bed',
    61: 'dining table',
    62: 'toilet',
    63: 'tv',
    64: 'laptop',
    65: 'mouse',
    66: 'remote',
    67: 'keyboard',
    68: 'cell phone',
    69: 'microwave',
    70: 'oven',
    71: 'toaster',
    72: 'sink',
    73: 'refrigerator',
    74: 'book',
    75: 'clock',
    76: 'vase',
    77: 'scissors',
    78: 'teddy bear',
    79: 'hair drier',
    80: 'toothbrush',
    81: 'ExposedBars'
}

SINGLE_CLS =  {
    0: 'background',
    1: 'ExposedBars'
}

class RebarDetector:
    def __init__(self):
        rospy.init_node('semantic_inference_node', anonymous=True)
        rospy.loginfo("Setting up node...")

        # Initialize the CvBridge
        self.bridge = CvBridge()
        self.segmentor = None
        self.classes = SINGLE_CLS

        # Get parameters containing the topics to use
        self.camera_image_topic = rospy.get_param('/camera_image_topic', 
                                                  '/image')
        self.inference_mask_topic = rospy.get_param('/inference_mask_topic', 
                                                    '/mask')
        self.inference_mask_img_topic = rospy.get_param('/inference_mask_img_topic',
                                                        '/mask_img')
        self.inference_rest_url = rospy.get_param('/inference_rest_url', 
                                                  'http://127.0.0.1:8000')

        try:
            from ultralytics import YOLO
            
            # TODO replace this by a rosparam for the weight path
            segment_weights_path = rospy.get_param('~segment_weights', 
                                                   'yolov8m-seg.pt')
            self.segmentor = YOLO(segment_weights_path)
            callback = self.image_torch_callback
            # Warmup the model
            [self.segmentor(
                np.zeros((480, 640, 3),
                         np.uint8)) for _ in range(3)]
        except ImportError as e:
            rospy.logwarn(
                'PyTorch model could not be imported, using REST API fallback')
            # Set the HTTP endpoint URL
            self.endpoint_url = f'{self.inference_rest_url}/segment'
            try:
                response = requests.get(f'{self.inference_rest_url}/status')
                response = response.json()
                callback = self.image_rest_callback
            except ConnectionRefusedError:
                rospy.logerror('REST Fallback is not reachable, exiting')
                rospy.signal_shutdown('Inference node coult not be initialized')

        
        # Subscribe to the image topic
        self.image_sub = rospy.Subscriber(self.camera_image_topic, Image,
                                          callback)
        self.mask_pub = rospy.Publisher(self.inference_mask_topic, Image)
        self.mask_img_pub = rospy.Publisher(self.inference_mask_img_topic, Image)
        self.cmap = np.asarray(self.create_colormap(len(self.classes.keys())))
        self._rng = np.random.default_rng(42)
        self._rng.shuffle(self.cmap[1:])
        rospy.loginfo("Node is ready to listen for incoming topics")

    @staticmethod
    def create_colormap(n):
        colormap = []

        colormap.append((0,0,0))
        for i in range(1, n+1):
            # Generate evenly spaced hues for distinct colors
            hue = i / n
            rgb_color = colorsys.hsv_to_rgb(hue, 1, 1)
            # Scale RGB values to 0-255
            rgb_color = tuple(int(val * 255) for val in rgb_color)
            colormap.append(rgb_color)

        return colormap
    
    def image_torch_callback(self, data):
        cv_image = self.bridge.imgmsg_to_cv2(data, 'bgr8')
        result = self.segmentor(cv_image)
        
        # (C, H, W)
        t1 = time.time()
        if result[0].masks is not None:
            predicted_masks = result[0].masks.data.cpu().numpy()
            predicted_classes = result[0].boxes.cpu().numpy().cls
            predicted_confs = result[0].boxes.cpu().numpy().conf
            mask = np.zeros((len(self.classes.keys()),
                                 *predicted_masks[0].shape))
            counts = np.zeros((len(self.classes.keys()),
                               *predicted_masks[0].shape),
                                dtype=np.uint32)
            for i, m in enumerate(predicted_masks):
                cls_id = predicted_classes[i].astype(int)
                mask[cls_id+1] += m*predicted_confs[i] #np.logical_or(mask[cls_id+1], m)
                counts[cls_id+1] += m.astype(np.uint32)
            
            # Avoid zero division
            counts[counts==0] = 1
            mask = mask / counts
            # Resize mask back to original size
            # Input: CHW, Result is H W C
            mask = mask.transpose((1,2,0))
            mask = cv2.resize(mask, 
                              dsize=result[0].orig_shape[::-1], 
                              interpolation=cv2.INTER_NEAREST)
            # Back to C, H, W
            mask = mask.transpose((2,0,1))
        else:
            mask = np.zeros((len(self.classes.keys()), *cv_image.shape[:-1]),
                        dtype=np.uint8)
        t2 = time.time()
        mask_assembly_time = t2-t1
        
        rospy.loginfo(f"mask assembly time: {mask_assembly_time}")
        self.publish(mask, result[0])
        

    def image_rest_callback(self, data):
        # try:
            # Convert the ROS Image message to a OpenCV image
        cv_image = self.bridge.imgmsg_to_cv2(data, 'bgr8')

        # Process the image (you can add your image processing code here)
        pil_img = PILImage.fromarray(cv_image)
        buffer = BytesIO()
        pil_img.save(buffer, 'png')
        encoded_img = b64encode(buffer.getvalue()).decode('utf8')

        # Prepare the data to be sent in the POST request
        data_to_send = {'data': encoded_img}
        # Make the POST request
        t1 = time.time()
        response = requests.post(self.endpoint_url, json=data_to_send)
        t2 = time.time()
        rospy.logdebug('Took {} seconds for request'.format(str(t2-t1)))


        # Check the response
        if response.status_code == 200:
            rospy.logdebug("POST request successful")
            
            t1 = time.time()
            mask_bytes = bytes(response.json()['data']).decode('utf8')
            t2 = time.time()
            rospy.logdebug('Parse successful in {} seconds'.format(str(t2-t1)))
            t1 = time.time()
            mask_bytes = b64decode(mask_bytes)
            t2 = time.time()
            rospy.logdebug('B64decode successful in {} seconds'.format(str(t2-t1)))
            
            t1 = time.time()
            mask = tifffile.imread(BytesIO(mask_bytes))
            t2 = time.time()
            rospy.logdebug("Imread successful in {} seconds".format(str(t2-t1)))
            
        else:
            rospy.logwarn("POST request failed with status code: {}".format(response.status_code))
        
        self.publish(mask)

        # except Exception as e:
        #     rospy.logerr("Error processing image and making POST request: {}".format(str(e)))
    def annotate(self, mask, result):
        """Visualizes inference result
        Only works with ultralytics inference result

        Args:
            mask (ndarray): Colored mask image
            result (ultralytics.engine.results.result): Inference result
        """
        # Retrieve original image
        canvas = result.orig_img.copy()
        
        # Add boxes
        boxes = result.boxes.cpu().numpy().data
        for box in boxes:
            bbox = box[:4].astype(int)
            conf = float(box[4])

            # Adjust to background class
            cls = int(box[5]) + 1
            canvas = cv2.rectangle(canvas, pt1=bbox[:2], pt2=bbox[2:],
                          color=self.cmap[cls].tolist(),
                          thickness=5,
            )
            (w, h), _ = cv2.getTextSize(
                            self.classes[cls] + f' {conf:.2f}',
                            cv2.FONT_HERSHEY_SIMPLEX, 2, 2)
            canvas = cv2.rectangle(canvas,
                                (bbox[0], bbox[1] - h),
                                (bbox[0] + w, bbox[1]),
                                self.cmap[cls].tolist(), -1) 
            canvas = cv2.putText(canvas, self.classes[cls] + f' {conf:.2f}',
                                 (bbox[0], bbox[1] - 5),
                    cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 2)
        canvas = cv2.addWeighted(mask, 0.5, canvas, 1, 0, None)
        return canvas.copy()

    def publish(self, mask, result, annotate=True):
        if len(mask.shape) == 3 and mask.shape[-1] > 1:
            # Pytorch/local inference mask
            mask_img = self.cmap[mask.argmax(axis=0).astype(np.uint8)].astype(np.uint8)
            mask = mask.argmax(axis=0).astype(np.uint8)
        else:
            # REST mask
            mask_img = self.cmap[mask.astype(np.uint8)].astype(np.uint8)

        if annotate:
            mask_img = self.annotate(mask_img, result)

        img_msg = self.bridge.cv2_to_imgmsg(mask_img, 'bgr8')
        self.mask_pub.publish(self.bridge.cv2_to_imgmsg(mask))
        self.mask_img_pub.publish(img_msg)

    def run(self):
        rospy.spin()

if __name__ == '__main__':
    node = RebarDetector()
    node.run()
