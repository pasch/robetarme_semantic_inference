# Semantic inference node based on YOLOv8

This repository contains code for a ROS node providing semantic inference
capabilities. It performs semantic segmentation and bounding box detection.

## Parameters
To control the topics for the node, use the following parameters:
| Parameter  | Default value   | Description   |
|-------------- | -------------- | -------------- |
| `/camera_image_topic`    | `/image`     | Name of the `sensor_msg/Image` topic that the node should subscribe to and run inference on|
| `/inference_mask_topic` | `/mask` | Name of the published topic containing the masks in (1, H, W) format | 
| `/inference_mask_img_topic` | `/mask_img` | Name of the published topic containing the image with inference results |

There is an additional parameter for the fallback REST API:

| Parameter  | Default value   | Description   |
|-------------- | -------------- | -------------- |
| `/inference_rest_url`    | `http://127.0.0.1:8000`     | URL for the fallback inference REST API |

## Communication structure
![ROS topic diagram](ros_node_comms.png)
Additional words:

- The `/mask` topic contains the most confident predicted class ID for each pixel in the input image.
- The `/mask_img` topic contains the inference image with the predictions drawn on top of it. (visualization)
- Replace the topic names with the one that were set through the parameter (or not)

## Deployment
The node is deployed in a Docker container to reach a maximum level of isolation
towards other nodes. This avoids conflicts regarding dependencies and such.
To deploy the node, execute the following commands in a shell.
This will build the container. **NB: The weight file must be in the repo directory or in a subdirectory**
 ```bash
 cd <path to this repo>
 docker build . --build-arg weight_file=<path_to_segment_weights.pt> \
        -t robetarme/semantic_inference:latest
 ```

This will run the container with GPU support and detach the shell. After approx. 10 seconds, 
the node should be ready to do inference.
```bash
docker run --network host --gpus all --name semantic_inference_node -d robetarme/semantic_inference:latest
```
A little explainer:
- `--network host` will remove the network isolation of the container. Thus, it has access to the network of the host.
  If all ROS-related containers are run with this argument, it facilitates easy communication.
- `--gpus all` will run the container with GPU support. Works only if the NVIDIA container runtime is installed.
- `--name` is to name the container and can be customized.
- `-d` detaches the container
